package entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Salarie implements Serializable {

    @Id
    private Long      numsal;

    private String   nomsal;
    private String   sexe;
    private Float     salaire;

    @ManyToOne
    @JoinColumn(name="CODEFONCT")
    private Fonction laFonction;

    @OneToMany(mappedBy = "leSalarie")
    private List<Affectation>  lesAffectations=new LinkedList<Affectation>();

    public Salarie() {}

    public Salarie(Long numsal, String nomsal, String sexe,Float salaire, Fonction laFonction) {
        this.numsal    =  numsal;
        this.nomsal    =  nomsal;
        this.salaire  = salaire;
        this.sexe       =  sexe;
        this.laFonction=laFonction;
        this.laFonction.getLesSalaries().add(this);
    }

    @Override
    public String toString() {
        return numsal+" "+nomsal+" "+sexe+" "+salaire;
    }


    public Fonction getLaFonction() {
        return laFonction;
    }
    public void setLaFonction(Fonction laFonction) {
        this.laFonction = laFonction;
    }
    public String getNomsal() {
        return nomsal;
    }
    public void setNomsal(String nomsal) {
        this.nomsal = nomsal;
    }
    public Long getNumsal() {
        return numsal;
    }
    public void setNumsal(Long numsal) {
        this.numsal = numsal;
    }
    public Float getSalaire() {
        return salaire;
    }
    public void setSalaire(Float salaire) {
        this.salaire = salaire;
    }
    public String getSexe() {
        return sexe;
    }
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public List<Affectation> getLesAffectations() {
        return lesAffectations;
    }
    public void setLesAffectations(List<Affectation> lesAffectations) {
        this.lesAffectations = lesAffectations;
    }
    
}
